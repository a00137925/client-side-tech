var minTemp = {
    label: "Temp - min (celsius)",
    data: temperatureMin,
    lineTension: 0.3,
    fill: false,
    borderColor: 'red',
    backgroundColor: 'transparent',
    pointBorderColor: 'red',
    pointBackgroundColor: 'lightgreen',
    pointRadius: 5,
    pointHoverRadius: 15,
    pointHitRadius: 30,
    pointBorderWidth: 2,
    pointStyle: 'rect'
  };

var maxTemp = {
    label: "Temp - max (celsius)",
    data: temperatureMax,
    lineTension: 0.3,
    fill: false,
    borderColor: 'purple',
    backgroundColor: 'transparent',
    pointBorderColor: 'purple',
    pointBackgroundColor: 'lightgreen',
    pointRadius: 5,
    pointHoverRadius: 15,
    pointHitRadius: 30,
    pointBorderWidth: 2
  };

var tempData = {
  labels: [moment().calendar(), moment().add(1, 'days').calendar(), moment().add(2, 'days').calendar(), moment().add(3, 'days').calendar(), moment().add(4, 'days').calendar()],
  datasets: [minTemp, maxTemp]
};

var chartOptions = {
  legend: {
    display: true,
    position: 'top',
    labels: {
      boxWidth: 80,
      fontColor: 'black'
    }
  }
};

var myChart = new Chart(ctx, {
  type: 'line',
  data: tempData,
  options: chartOptions
});



var myChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: [moment().calendar(), moment().add(1, 'days').calendar(), moment().add(2, 'days').calendar(), moment().add(3, 'days').calendar(), moment().add(4, 'days').calendar()],
    datasets: [{
      label: '5 Day Forecast',
      data: temperatureMin, temperatureMax,
      backgroundColor: [
        'rgba(255, 153, 0, 0.5)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 1
    }]
  },
  options: {responsive: false,
    scales: {
      title: 'Hi',
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: "Speed in Miles per Hour",
          fontColor: "green"
        },
        ticks: {
          beginAtZero:true
          }
      }]
    }
  }
});


for(var i=0; i<30; i+=6){
    temperatureMin[j] = parseInt(data1.getElementsByTagName("temperature")[i].getAttribute('min'));
    j++;
}

var temperatureMax = [];
var k =0;
for(var i=0; i<30; i+=6){
  //console.log(data1.getElementsByTagName("temperature")[i].getAttribute('value'));
    temperatureMax[k] = parseInt(data1.getElementsByTagName("temperature")[i].getAttribute('max'));
    k++;
}
