"use strict";

//Times - 00.00, 03.00, 06.00, 09.00, 12.00, 15.00, 18.00, 21.00

var $ = function(id) {
    return document.getElementById(id);
};

//---- Build each part of the URL for the API request
var urlA = "http://api.openweathermap.org/data/2.5/forecast?q=";
var urlB = "&mode=xml&units=metric";
var appID = "&appid=b54b7c586543f69abb8cb1dffb9fa446";

//---- This the chart variable
var myChart;
var myChart2;

var check=true;

//---- This is the core function; it organizes the data from the API xml response
//---- into neat arrays to be used for the chart and weather info display area
var createChart = function(data1) {
  //---- If a chart has already been made, destroy it.
  //---- Otherwise it remains behind the new one and causes problems with the interface
  if(typeof myChart != "undefined") myChart.destroy();
  if(typeof myChart2 != "undefined") myChart2.destroy();

  //---- Get the canvas where the chart will be displayed
  var ctx = $("myChart");
  var ctx2 = $("myChart2");
  //---- Gets the city name from the xml
  var city = data1.getElementsByTagName("name")[0].firstChild.nodeValue;
  //console.log(city);
  var country = data1.getElementsByTagName("country")[0].firstChild.nodeValue;
  //console.log(country);

  //---- Displays the city and contry code on the webpage
  $("locationTitle").innerText = city+", "+country;

  //---- Declare the variables that will be used here for storing values and keeping count
	var temperatureMin = [];
  var temperatureMax = [];
  var min = 100;
  var max = 0;
  var count = 0;
	var j =0;
  var iconCode= [];
  var humidity = [];
  var windDir = [];
  var windType = [];
  var rain=[];
  var time = [];
  var timeTo = [];
  var checkM, checkD, checkN = false;

  //---- This loop runs thru all 40 3hr timeslots over 5 days, gathering data to be used in the chart
	for(var i=0; i<40; i++){
      //---- Get a min/max temp value every 3hrs
      var xmlMin = parseInt(data1.getElementsByTagName("temperature")[i].getAttribute('min'));
      var xmlMax = parseInt(data1.getElementsByTagName("temperature")[i].getAttribute('max'));
      //---- Get the time period for each slot, slice it down to two digits and convert into int
      time[i] = parseInt(data1.getElementsByTagName("time")[i].getAttribute('from').slice(11, 13));
      timeTo[i] = parseInt(data1.getElementsByTagName("time")[i].getAttribute('to').slice(11, 13));

      //console.log("Count: "+i);
      //console.log("Time: "+time[i]);

      //---- Compare current min value with preveiously set one
      if(xmlMin<min)
      {
        min=xmlMin;
      }
        //---- Compare current max value with preveiously set one
      if(xmlMax>max)
      {
        max=xmlMax;
      }

      //---- If the morning radio button is checked, display morning data for the days
      if($("morning").checked)
      {
        //---- An intial check for the first day, in case it's past the selected period.
        //---- If it is, just set the current values for the day. This is repeated for day and night
        if(!checkM)
        {
          if(time[0]>9 || time[0]==9)
          {
            windDir[0] = data1.getElementsByTagName("windDirection")[i].getAttribute('name');
            windType[0] = data1.getElementsByTagName("windSpeed")[i].getAttribute('name');
            humidity[0] = data1.getElementsByTagName("humidity")[i].getAttribute('value');
            if(data1.getElementsByTagName("precipitation")[i].getAttribute('value') == null)
            rain[0]=0;
            else rain[0] = data1.getElementsByTagName("precipitation")[i].getAttribute('value');
            temperatureMin[0] = min;
            temperatureMax[0] = max;
            min=100;
            max=0;
            iconCode[0] = data1.getElementsByTagName("symbol")[i].getAttribute('var');
            j++;
          }
          checkM=true;
        }
        else if(time[i]==9)
        {
          windDir[j] = data1.getElementsByTagName("windDirection")[i].getAttribute('name');
          windType[j] = data1.getElementsByTagName("windSpeed")[i].getAttribute('name');
          humidity[j] = data1.getElementsByTagName("humidity")[i].getAttribute('value');
          if(data1.getElementsByTagName("precipitation")[i].getAttribute('value') == null)
          rain[j]=0;
          else rain[j] = data1.getElementsByTagName("precipitation")[i].getAttribute('value');
          temperatureMin[j] = min;
          temperatureMax[j] = max;
          min=100;
          max=0;
          iconCode[j] = data1.getElementsByTagName("symbol")[i].getAttribute('var');
          j++;
        }
      }
      //---- If the day radio button is checked, display daytime data for the days
      if($("day").checked)
      {
        if(!checkD)
        {
          if(time[0]>15 || time[0]==15)
          {
            windDir[0] = data1.getElementsByTagName("windDirection")[i].getAttribute('name');
            windType[0] = data1.getElementsByTagName("windSpeed")[i].getAttribute('name');
            humidity[0] = data1.getElementsByTagName("humidity")[i].getAttribute('value');
            if(data1.getElementsByTagName("precipitation")[i].getAttribute('value') == null)
            rain[0]=0;
            else rain[0] = data1.getElementsByTagName("precipitation")[i].getAttribute('value');
            temperatureMin[0] = min;
            temperatureMax[0] = max;
            min=100;
            max=0;
            iconCode[0] = data1.getElementsByTagName("symbol")[i].getAttribute('var');
            j++;
          }
          checkD=true;
        }
        else if(time[i]==15)
        {
          windDir[j] = data1.getElementsByTagName("windDirection")[i].getAttribute('name');
          windType[j] = data1.getElementsByTagName("windSpeed")[i].getAttribute('name');
          humidity[j] = data1.getElementsByTagName("humidity")[i].getAttribute('value');
          if(data1.getElementsByTagName("precipitation")[i].getAttribute('value') == null)
          rain[j]=0;
          else rain[j] = data1.getElementsByTagName("precipitation")[i].getAttribute('value');
          temperatureMin[j] = min;
          temperatureMax[j] = max;
          min=100;
          max=0;
          iconCode[j] = data1.getElementsByTagName("symbol")[i].getAttribute('var');
          j++;
        }
      }
      //---- If the night radio button is checked, display nighttime data for the days
      if($("night").checked)
      {
        if(!checkN)
        {
          if(time[0]>21 || time[0]==21)
          {
            windDir[0] = data1.getElementsByTagName("windDirection")[i].getAttribute('name');
            windType[0] = data1.getElementsByTagName("windSpeed")[i].getAttribute('name');
            humidity[0] = data1.getElementsByTagName("humidity")[i].getAttribute('value');
            if(data1.getElementsByTagName("precipitation")[i].getAttribute('value') == null)
            rain[0]=0;
            else rain[0] = data1.getElementsByTagName("precipitation")[i].getAttribute('value');
            temperatureMin[0] = min;
            temperatureMax[0] = max;
            min=100;
            max=0;
            iconCode[0] = data1.getElementsByTagName("symbol")[i].getAttribute('var');
            j++;
          }
          checkN=true;
        }
        else if(time[i]==21)
        {
          //console.log(i);
          //console.log(time[i]);
          windDir[j] = data1.getElementsByTagName("windDirection")[i].getAttribute('name');
          windType[j] = data1.getElementsByTagName("windSpeed")[i].getAttribute('name');
          humidity[j] = data1.getElementsByTagName("humidity")[i].getAttribute('value');
          if(data1.getElementsByTagName("precipitation")[i].getAttribute('value') == null)
          rain[j]=0;
          else rain[j] = data1.getElementsByTagName("precipitation")[i].getAttribute('value');
          temperatureMin[j] = min;
          temperatureMax[j] = max;
          min=100;
          max=0;
          iconCode[j] = data1.getElementsByTagName("symbol")[i].getAttribute('var');
          j++;
        }

      }
	}


	console.log(rain);

  //---- Easier to edit arrays that store the information and options for the chart
  var minTemp = {
      label: "Temp - min (celsius)",
      data: temperatureMin,
      lineTension: 0.3,
      fill: true,
      borderColor: 'blue',
      backgroundColor: 'rgba(162, 189, 232, 0.4)',
      pointBorderColor: 'blue',
      pointBackgroundColor: 'orange',
      pointRadius: 5,
      pointHoverRadius: 15,
      pointHitRadius: 30,
      pointBorderWidth: 2,
      pointStyle: 'rect'
    };

  var maxTemp = {
      label: "Temp - max (celsius)",
      data: temperatureMax,
      lineTension: 0.3,
      fill: true,
      borderColor: 'orange',
      backgroundColor: 'rgba(255, 153, 0, 0.5)',
      pointBorderColor: 'red',
      pointBackgroundColor: 'orange',
      pointRadius: 5,
      pointHoverRadius: 15,
      pointHitRadius: 30,
      pointBorderWidth: 2
    };

  var tempData = {
    labels: [moment().format('dddd'), moment().add(1, 'days').format('dddd'), moment().add(2, 'days').format('dddd'), moment().add(3, 'days').format('dddd'), moment().add(4, 'days').format('dddd')],
    datasets: [minTemp, maxTemp]
  };

  var chartOptions = { responsive: false,
    legend: {
      display: true,
      position: 'top',
      labels: {boxWidth: 40,fontColor: 'black'}
    },
    scales: {
      title: 'Temperature',
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: "Temperature in Degress Celsius",
          fontColor: "#543a0f"
        },
        ticks: {
          beginAtZero:true
          }
      }],
      xAxes: [{
        scaleLabel: {
          display: true,
        },
        ticks: {
          beginAtZero:true,
          fontColor: "#543a0f"
          }
      }]
    }
  };

  //---- Create the chart with all the data and options from above
  myChart = new Chart(ctx, {
    type: 'line',
    data: tempData,
    options: chartOptions
  });

 //---- Alternative way for making a chart (rainfall chart)
  myChart2 = new Chart(ctx2, {
    type: 'bar',
    data: {
      labels: [moment().format('dddd'), moment().add(1, 'days').format('dddd'), moment().add(2, 'days').format('dddd'), moment().add(3, 'days').format('dddd'), moment().add(4, 'days').format('dddd')],
      color: "black",
      datasets: [
        {
          label: "Rainfall in mm",
          backgroundColor: "#EAD8B4",
          data: rain,
          fontColor: "#3cba9f"
        }
      ]
    },
    options: {
      legend: { display: false },
      title: {
        display: true,
        text: 'Rainfall',
        fontColor: "black",
      },
      scales: {
        title: 'Rainfall',
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: "Rainfall in mm",
            fontColor: "#543a0f"
          },
          ticks: {
            beginAtZero:true
            }
        }],
        xAxes: [{
          scaleLabel: {
            display: true,
          },
          ticks: {
            beginAtZero:true,
            fontColor: "#543a0f"
            }
        }]
      }
    }
});

  //---- This organizes the data to be used for the days in the tabbed area
  var temp =[];
  for(var i=0; i<5; i++)
  {
    temp[i]=((temperatureMin[i]+temperatureMax[i])/2);
    $("temp"+(i+1)).innerText = temp[i]+"°C";

    $("humidity"+(i+1)).innerText = humidity[i]+"%";

    $("wind"+(i+1)).innerText = windType[i]+", "+windDir[i];
  }
  //console.log(temp[3]);
  //console.log(temp[4]);

  //---- Getting relevenat icons from the API to display in tabbed area
  //---- These provide a visual depiction of cloud and rain for each day
  var iconURL;
  try
  {
    for(var i=0; i<iconCode.length; i++)
    {
      iconURL = "http://openweathermap.org/img/w/" + iconCode[i] + ".png";
      $('img'+(i+1)).setAttribute('src', iconURL);
    }
  }
  catch(err){}

}


//---- Function that sets up HTTP Object request, in this case, an XML file request.
//---- Also provides fallbacks for older browsers
var getHTTPObject = function() {
  var xhr = false;
  if (window.XMLHttpRequest) {
    xhr = new XMLHttpRequest();
  } else if (window.ActiveXObject) {
    try {
      xhr = new ActiveXObject("Msxml2.XMLHTTP");
    } catch(e) {
      try {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
      } catch(e) {
        xhr = false;
      }
    }
  }
  return xhr;
}

//---- Where the HTTP request is built and sent
var getWeather = function() {
	var request = getHTTPObject();

	var city = $("location").value;

	var completeURL = urlA+city+urlB+appID;

	if (request) {
		request.onreadystatechange = function() {
		parseResponse(request);
		};

		request.open("GET", completeURL, true);
		request.send(null);
  }

  //---- Gets the tabbed area display set
  $("tabbedArea").style.display = "inline-grid";
  $("chartTab").style.display = "inline-grid";

  //---- Display a chart and info on first run
  if(check)
  {
    $("chartArea").style.display = "block";
    $("day1").style.display = "block";
    check=false;
  }
  var tablinks = document.getElementsByClassName("tablinks");

  for (var i = 2; i < tablinks.length; i++)
  {
      tablinks[i].innerHTML = moment().add(i, 'days').format('dddd');
  }

}

//---- Parses and deals with the HTTP response
//---- It's here, after all the data is ready, that the creatChart function is called
var parseResponse = function(request) {
  if (request.readyState == 4) {
    if (request.status == 200 || request.status == 304) {
      var response = document.getElementById("response");

	  var data = request.responseXML;
	  console.log(data);
	  createChart(data);


    }
  }
}

//---- Function controlling the display of the tabbed area content
function openCity(evt, day) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(day).style.display = "block";
    evt.currentTarget.className += " active";
}

function openChart(evt, chart) {
    // Declare all variables
    var i, tabcontent2, charts;

    // Get all elements with class="tabcontent2" and hide them
    tabcontent2 = document.getElementsByClassName("tabcontent2");
    for (i = 0; i < tabcontent2.length; i++) {
        tabcontent2[i].style.display = "none";
    }

    // Get all elements with class="charts" and remove the class "active"
    charts = document.getElementsByClassName("charts");
    for (i = 0; i < charts.length; i++) {
        charts[i].className = charts[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(chart).style.display = "block";
    evt.currentTarget.className += " active";
}

var check = function()
{
  if($('location').value=="") alert("Please enter a city!")
  else getWeather();
}



window.onload = function() {
    $("get").onclick = check;
    $("location").focus();
	//createChart();
};
