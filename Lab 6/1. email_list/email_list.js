"use strict";
var $ = function(id) {
    return document.getElementById(id);
};
var joinList = function() {
    var emailAddress1 = $("email_address1").value;
    var emailAddress2 = $("email_address2").value;
    var firstName = $("first_name").value;
    var isValid = true;
	
	var spans =document.getElementsByTagName("span");
	var emailMsg1 = spans[0];
	var emailMsg2 = spans[1];
	var nameMsg = spans[2];

    // validate the first entry
    if (emailAddress1 === "") { 
        emailMsg1.firstChild.nodeValue = "This field is required.";
        isValid = false;
    } else {
        emailMsg1.firstChild.nodeValue = "";
    } 

    // validate the second entry
    if (emailAddress2 === "") { 
        emailMsg2.firstChild.nodeValue = "This field is required.";
        isValid = false; 
    } else if (emailAddress1 !== emailAddress2) { 
        emailMsg2.firstChild.nodeValue = "This entry must equal first entry.";
        isValid = false;
    } else {
        emailMsg2.firstChild.nodeValue = "";
    }

    // validate the third entry  
    if (firstName === "") {
        nameMsg.firstChild.nodeValue = "This field is required.";
        isValid = false;
    } else {
        nameMsg.firstChild.nodeValue = "";
    }  

    // submit the form if all entries are valid
    if (isValid) {
        $("email_form").submit(); 
    }
};

window.onload = function() {
    $("join_list").onclick = joinList;
    $("email_address1").focus();
};
