"use strict";
var $ = function(id) { return document.getElementById(id); };


var clearTextBoxes = function() 
{
    $("degrees_entered").value = "";
    $("degrees_computed").value = "";
};

var toFahrenheit = function() {
	clearTextBoxes();
   $("degree_label_1").firstChild.nodeValue = "Enter C degrees: ";
   $("degrees_entered").required = true;
   $("degree_label_2").firstChild.nodeValue = "Degrees Fahrenheit: ";
   $("degrees_entered").focus();
};

var toCelsius = function() {
   clearTextBoxes();
   $("degree_label_1").firstChild.nodeValue = "Enter F degrees: ";
   $("degrees_entered").required = true;
   $("degree_label_2").firstChild.nodeValue = "Degrees Celsius: ";
   $("degrees_entered").focus();
   
};

var convertTemp = function() {
	if (isNaN($("degrees_entered").value)){
		alert("Please enter a valid number");	
}
	
	
		
	if ($("to_celsius").checked){
		
		var degreesEntered = $("degrees_entered").value;
		 $("degrees_computed").value = Math.round((degreesEntered - 32)*5/9);
		 
	}
	else{
		var degreesEntered = $("degrees_entered").value;
		 $("degrees_computed").value = Math.round((degreesEntered *9/5)+32);
		
	}
	
};

window.onload = function() {
	
	
    $("convert").onclick = convertTemp;
    $("to_celsius").onclick = toCelsius;
    $("to_fahrenheit").onclick = toFahrenheit;
	$("degrees_entered").focus();
};

