var periods;
var json_File;

var $ = function (id) {
	return document.getElementById(id);
}

var C = function (element) {
	return document.createElement(element);
}

var lat;
var lon;

var getHTTPObject = function () {
	var xhr = false;
	if (window.XMLHttpRequest) {
		xhr = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		try {
			xhr = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
				xhr = false;
			}
		}
	}
	return xhr;
}

var getWeatherURL = function (url) {
	var request = getHTTPObject();
	if (request) {
		request.onreadystatechange = function () {
			parseResponse(request);
		};
		request.open("GET", url, true);
		request.send(null);
	}
}

var parseResponse = function (request) {
	if (request.readyState == 4)
		if (request.status == 404) {
			var tabs = $("WeatherPanel1");
			//cleaning all to prevent multiple adding of tables if new Search happen
			while (tabs.hasChildNodes()) {
				tabs.removeChild(tabs.firstChild);
			}
			var city = $("city").value.toUpperCase();
			var main = $("header");
			if ($("Location") !== null) {
				main.removeChild(main.lastChild);
			}
			var google = $("googleMap");
			google.style.opacity = "0";
			google.style.width = "0%";	
			var div1 = C("div");
			div1.id = "Location";
			main.appendChild(div1);
			var posit = C("h1");
			posit.style.lineheight = "100%";
			posit.appendChild(document.createTextNode("SORRY, but we could not find the city: [ " + city + " ] in our database..."));
			div1.appendChild(posit);
			div1.style.border = "9px solid cyan";
			div1.style.borderRadius = "30px";
			div1.style.backgroundColor = "blue";
			div1.style.color = "white";
			div1.style.fontweight = "bold";
			div1.style.height = "205px";
			div1.style.width = "30%";
			div1.style.padding = "10px 0 0 30px";
		}
		if (request.status == 200 || request.status == 304) {
			console.log(request.responseText);
			//Parse data from XML and add to webpage
			json_File = JSON.parse(request.responseText);
			
			lat = json_File.city.coord.lat;
			lon = json_File.city.coord.lon;
			console.log(lat);
			var google = $("googleMap");
			google.style.width = "35%";
			google.style.height = "215px";
			google.style.border = "9px solid cyan";
			google.style.borderRadius = "30px";
			google.style.opacity = "0.7";
			myMap(lat, lon);

			var main = $("header");
			if ($("Location") !== null) {
				main.removeChild(main.lastChild);
			}
			var div1 = C("div");
			div1.id = "Location";
			main.appendChild(div1);
			var posit = C("h1");
			posit.style.lineHeight = "100%";
			var loc = json_File.city.name;
			posit.appendChild(document.createTextNode("Location: " + loc));
			var count = C("h1");
			count.style.lineHeight = "100%";
			var country = json_File.city.country;
			count.appendChild(document.createTextNode("Country: " + country));
			var lati = C("h2");
			var longi = C("h2");
			longi.style.lineHeight = "0%";
			lati.appendChild(document.createTextNode("Latitude: " + lat));
			longi.appendChild(document.createTextNode("Longitude: " + lon));
			div1.appendChild(posit);
			div1.appendChild(count);
			var bre = C("br");
			div1.appendChild(lati);
			div1.appendChild(bre);
			div1.appendChild(longi);
			div1.style.border = "9px solid cyan";
			div1.style.borderRadius = "30px";
			div1.style.backgroundColor = "blue";
			div1.style.color = "white";
			div1.style.fontWeight = "bold";
			div1.style.height = "205px";
			div1.style.width = "20%";
			div1.style.padding = "10px 0 0 30px";
			div1.style.opacity = "0.6";

			var tabs = $("WeatherPanel1");
			//cleaning all to prevent multiple adding of tables if new Search happen
			while (tabs.hasChildNodes()) {
				tabs.removeChild(tabs.firstChild);
			}

			periods = json_File.list;
			var day = periods[0].dt_txt;
			var date = day.split(" ");
			var lastDate;
			for (i = 0; i < periods.length; i++) {
				lastDate = date[0];
				var z = 0;

				var minTdata = [];
				var maxTdata = [];
				var water = [];
				var xLabels = [];

				var div = C("div");
				var t = C("table");
				div.appendChild(t);
				var h = C("thead");
				var b = C("tbody");
				tabs.appendChild(div);
				t.appendChild(h);
				t.appendChild(b);
				div.style.float = "none";
				div.style.display = "flex";

				var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
				var d = new Date(date[0]);
				var dayName = days[d.getDay()];
				var r1 = h.insertRow(z);
				var heading1 = C("td");
				heading1.appendChild(document.createTextNode(dayName));
				var heading2 = C("td");
				var datum = date[0].split("-");
				heading2.appendChild(document.createTextNode(datum[2] + ". " + datum[1] + "."));
				var blank = C("td");
				blank.appendChild(document.createTextNode(""));
				var blank1 = C("td");
				blank1.appendChild(document.createTextNode(""));
				var blank2 = C("td");
				blank2.appendChild(document.createTextNode(""));
				r1.appendChild(blank);
				r1.appendChild(blank1);
				r1.appendChild(blank2);
				r1.appendChild(heading1);
				r1.appendChild(heading2);
				var r2 = b.insertRow(z);
				var first = C("th");
				first.appendChild(document.createTextNode("Time (from:)"));
				var second = C("th");
				second.appendChild(document.createTextNode("Img"));
				var third = C("th");
				third.appendChild(document.createTextNode("Temp.(°C)"));
				var fourth = C("th");
				fourth.appendChild(document.createTextNode("Clouds - Rain - Snow (cm)"));
				var fifth = C("th");
				fifth.appendChild(document.createTextNode("Wind - Speed (m/s)"));
				var sixth = C("th");
				sixth.appendChild(document.createTextNode("Hum.(%)"));
				var seventh = C("th");
				seventh.appendChild(document.createTextNode("Press.(hPa)"));
				r2.appendChild(first);
				r2.appendChild(second);
				r2.appendChild(third);
				r2.appendChild(fourth);
				r2.appendChild(fifth);
				r2.appendChild(sixth);
				r2.appendChild(seventh);
				while (lastDate == date[0]) {
					z++;
					var r = b.insertRow(z);
					var imag = periods[i].weather[0].icon;
					var img = document.createElement('img');
					img.src = "images/" + imag + ".png";
					var time = date[1].split(":");
					var tm = C("td");
					tm.style.fontSize = "120%";
					tm.style.fontWeight = "bold";
					var hour = time[0] + ": " + time[1];
					tm.appendChild(document.createTextNode(hour));
					xLabels.push(hour);
					var t = C("td");
					var mint = C("pre");
					var maxt = C("pre");
					var min = periods[i].main.temp_min;
					var max = periods[i].main.temp_max;
					mint.appendChild(document.createTextNode("min:  " + min));
					maxt.appendChild(document.createTextNode("max:  " + max));
					t.appendChild(mint);
					t.appendChild(maxt);
					minTdata.push(min);
					maxTdata.push(max);
					var ra = C("td");
					var cloud = periods[i].weather[0].main.toUpperCase();
					var rain = periods[i].weather[0].description;
					var precip;
					if (periods[i].hasOwnProperty('rain')) {
						if (periods[i].rain.hasOwnProperty('3h')) {
							precip = periods[i].rain['3h'];
						} else {
							precip = 0;
						}
					} else if (periods[i].hasOwnProperty('snow')) {
						if (periods[i].snow.hasOwnProperty('3h')) {
							precip = periods[i].snow['3h'];
						} else {
							precip = 0;
						}
					} else {
						precip = 0;
					}
					precip = parseFloat(precip);
					precip = precip.toFixed(2);
					var cloudP = C("pre");
					var rainP = C("pre");
					if (rain.toUpperCase() === cloud) {
						cloudP.appendChild(document.createTextNode(cloud));
						rainP.appendChild(document.createTextNode(precip));
					} else {
						cloudP.appendChild(document.createTextNode(cloud));
						rainP.appendChild(document.createTextNode(" ( " + rain + " ):  " + precip));
					}
					ra.appendChild(cloudP);
					ra.appendChild(rainP);
					water.push(precip);
					var w = C("td");
					var dir = parseInt(periods[i].wind.deg) + " °";
					//var sp = N("windSpeed", i, 0, "name").toUpperCase();
					var speed = periods[i].wind.speed;
					/*if (dir == "") {
					dir = "No Dir.";
					}*/
					var dirP = C("pre");
					var spP = C("pre");
					spP.appendChild(document.createTextNode(speed));
					dirP.appendChild(document.createTextNode(dir));
					w.appendChild(spP);
					w.appendChild(dirP);
					var h = C("td");
					var hum = periods[i].main.humidity;
					h.appendChild(document.createTextNode(hum));
					var p = C("td");
					var press = periods[i].main.pressure;
					press = parseInt(press);
					p.appendChild(document.createTextNode(press));

					r.appendChild(tm);
					var image = r.insertCell(-1);
					image.appendChild(img);
					r.appendChild(t);
					r.appendChild(ra);
					r.appendChild(w);
					r.appendChild(h);
					r.appendChild(p);

					i++;

					if (periods[i] == undefined) {
						lastDate = date[1];
					} else {
						day = periods[i].dt_txt;
						date = day.split(" ");
					}
					if (lastDate != date[0]) {
						i--;
					}
				}
				if (periods[i + 1] !== undefined) {
					xLabels.push("24: 00");
					var miT = periods[i + 1].main.temp_min;
					var maT = periods[i + 1].main.temp_max;
					minTdata.push(miT);
					maxTdata.push(maT);

					var precL;
					if (periods[i+1].hasOwnProperty('rain')) {
						if (periods[i+1].rain.hasOwnProperty('3h')) {
							precL = periods[i+1].rain['3h'];
						} else {
							precL = 0;
						}
					} else if (periods[i+1].hasOwnProperty('snow')) {
						if (periods[i+1].snow.hasOwnProperty('3h')) {
							precL = periods[i+1].snow['3h'];
						} else {
							precL = 0;
						}
					} else {
						precL = 0;
					}
					precL = parseFloat(precL);
					precL = precL.toFixed(2);
					water.push(precL);
				}
				var canvas = C("canvas");
				var divC = C("div");
				divC.appendChild(canvas);
				div.appendChild(divC);
				divC.style.width = "48%";
				divC.style.display = "inline";
				divC.style.margin = "0 0 33px 30px";
				divC.style.border = "2px solid black";
				divC.style.borderRadius = "10px";
				divC.style.backgroundColor = "#777";
				divC.style.padding = "20px 20px 10px 10px";
				divC.style.opacity = "0.8";

				var ctxChart = canvas.getContext('2d');
				var myChart = new Chart(ctxChart, {
						type: 'line',
						data: {
							labels: xLabels,
							datasets: [{
									label: 'Min Temperature',
									yAxisID: 'A',
									backgroundColor: "rgba(150,40,255,0.5)",
									borderColor: "rgba(125,38,205,1)",
									data: minTdata
								}, {
									label: 'Max Temperature',
									yAxisID: 'A',
									backgroundColor: "rgba(255,30,30,0.6)",
									borderColor: "rgba(255,0,0,1)",
									data: maxTdata
								}, {
									label: 'Precipitation',
									yAxisID: 'B',
									data: water,
									backgroundColor: "rgba(0,191,255,0.8)",
									borderColor: "rgba(0,0,255,1)"
								}
							]
						},
						options: {
							maintainAspectRatio: false,
							title: {
								display: true,
								fontColor: "cyan",
								fontSize: 24,
								text: dayName + ": " + datum[2] + ". " + datum[1] + "."
							},
							scales: {
								scaleFontColor: "#ffffff",
								xAxes: [{
										scaleLabel: {
											display: true,
											labelString: "Time during the Day",
											fontColor: "white"
										},
										gridLines: {
											color: "white",
											lineWidth: 0.5
										},
										ticks: {
											fontColor: "white",
										}
									}
								],
								yAxes: [{
										id: 'A',
										title: "Temperature (°C)",
										position: "left",
										//stacked: true,
										display: true,
										gridLines: {
											color: "white",
											lineWidth: 0.5,
											display: false
										},
										ticks: {
											fontColor: "white",
											beginAtZero: true
										},
										scaleLabel: {
											display: true,
											labelString: "Temperature (°C)",
											fontColor: "white",
										}
									}, {
										id: 'B',
										position: "right",
										//stacked: false,
										display: true,
										gridLines: {
											color: "white",
											lineWidth: 0.5
										},
										ticks: {
											fontColor: "white",
											beginAtZero: true
										},
										scaleLabel: {
											display: true,
											labelString: "Precipitation (cm)",
											fontColor: "white",
										}
									}
								]
							},
							legend: {
								labels: {
									fontColor: "white"
								}
							}
						}
					});
			}
		}
}

function getLocation() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPosition);
	} else {
		x.innerHTML = "Geolocation is not supported by this browser.";
	}
}
function showPosition(position) {
	lat = position.coords.latitude;
	lon = position.coords.longitude;
	console.log(lat);
	console.log(lon);
	// Use the API key (AppID) you get when you register with openweather.org
	var url = "http://api.openweathermap.org/data/2.5/forecast?lat=" + lat + "&lon=" + lon + "&units=metric&APPID=4325ca1c9dd19b30c2ecab945d5a0ab2";
	var google = $("googleMap");
	google.style.width = "35%";
	google.style.height = "215px";
	google.style.border = "9px solid cyan";
	google.style.borderRadius = "30px";
	google.style.opacity = "0.7";
	myMap(lat, lon);
	getWeatherURL(url);
}

function myMap(lat, lon) {
	var mapProp = {
		center: new google.maps.LatLng(lat, lon),
		zoom: 13,
	};

	var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
	var marker = new google.maps.Marker({
			position: new google.maps.LatLng(lat, lon),
			map: map
		});
}

window.onload = function () {

	$("searchButton").onclick = function () {
		var city = $("city").value;
		console.log(city);
		// Use the API key (AppID) you get when you register with openweather.org
		var url = "http://api.openweathermap.org/data/2.5/forecast?q=" + city + "&units=metric&APPID=4325ca1c9dd19b30c2ecab945d5a0ab2";
		getWeatherURL(url);
	}
	$("city").focus();

	$("coordButton").onclick = function () {
		getLocation();
	}
	$("city").focus();
}
