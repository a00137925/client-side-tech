"use strict";

var $ = function(id) {
    return document.getElementById(id);
};

var baseUrlF = "http://api.openweathermap.org/data/2.5/forecast?q=";
var urlQueryF = "&mode=xml&units=metric";
var appIDF = "&appid=84fd9bd7a4a5c4552ec6d185d92a4f6e";


var createChart = function(dataZ) {
var ctx = $("myChart");

var getDDMenu = $("dropDownMenu");
var menuChoice = getDDMenu.options[getDDMenu.selectedIndex].value;
var menuChoiceText = getDDMenu.options[getDDMenu.selectedIndex].text;


console.log(menuChoice);


// 5 day forecast at current time
// var temperature = [];
//
// var j =0;
// for(var i=0; i<40; i+=8){
//   //console.log(dataZ.getElementsByTagName("time")[i].getAttribute('from'));
//   temperature[j] = parseInt(dataZ.getElementsByTagName("temperature")[i].getAttribute('value'));
//     j++;
// }

// 5 day forecast at time chosen
var temperature2 = [];
var j2 =0;
var dateLabels = [];

for(var i=0; i<40; i++){
  // added menuChoice for a user to choose the time to display
  if (dataZ.getElementsByTagName("time")[i].getAttribute('from').includes(menuChoice)){
    temperature2[j2] = parseInt(dataZ.getElementsByTagName("temperature")[i].getAttribute('value'));
    dateLabels[j2] = dataZ.getElementsByTagName("time")[i].getAttribute('from');
    j2++;
  }
  console.log(dateLabels);
}


var myChart = new Chart(ctx, {
  type: 'line',
  data: {
    // scale the labels based on the current day
  //   moment().calendar(),moment().add(1, 'days').calendar(),moment().add(2, 'days').calendar(), moment().add(3, 'days').calendar()
  // , moment().add(4, 'days').calendar()
    labels: dateLabels,
    datasets: [{
      label: '5 day forecast ' + menuChoiceText,
      data: temperature2,//[temperature[2], 19, 3, 5, 2, 3],
      backgroundColor: [
        'rgb(245, 245, 0,0.4)',

      ],
      borderColor: [
        'rgb(128, 255, 0)'
      ],
      borderWidth: 3
    }]
  },
  options: {
    legend: {
            labels: {
                fontColor: "orange",
                fontSize: 18
            }
        },
        scales: {
                yAxes: [{
                  scaleLabel: {  display: true,  labelString: 'oC'  },
                    ticks: {fontSize: 18, fontFamily: "'Roboto', sans-serif", fontColor: 'orange'},
                    gridLines: {
                    color: "#80ff00"
                                },
                }],
                xAxes: [{
                  scaleLabel: {  },
                    ticks: {fontSize: 18, fontFamily: "'Roboto', sans-serif", fontColor: 'orange'}
                }]
            }
  }
});
}


var getHTTPObject = function() {
  var xhr = false;
  if (window.XMLHttpRequest) {
    xhr = new XMLHttpRequest();
  } else if (window.ActiveXObject) {
    try {
      xhr = new ActiveXObject("Msxml2.XMLHTTP");
    } catch(e) {
      try {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
      } catch(e) {
        xhr = false;
      }
    }
  }
  return xhr;
}


var getWeather = function() {
	var request = getHTTPObject();
	var city = $("location").value;

	var completeURL = baseUrlF+city+urlQueryF+appIDF;

if (request) {
  request.onreadystatechange = function() {
  parseResponse(request);
  };

		request.open("GET", completeURL, true);
		request.send();
  }
}

var parseResponse = function(request) {
  if (request.readyState == 4) {
    if (request.status == 404){
      alert("Error 404 cannot find this city");
    } else if (request.status == 200 || request.status == 304) {
      var response = document.getElementById("response");

  	  var data = request.responseXML;
  	  console.log(data);
  	  createChart(data);
      document.getElementById('myChart').scrollIntoView();

    }
  }
}

window.onload = function() {
    $("get").onclick = getWeather;
    $("location").focus();
};
