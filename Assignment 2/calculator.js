"use strict";
var $ = function(id) {
  return document.getElementById(id);
};

var spoons = 'spoons';
var check = false;

var calulate = function(){
  var input = $('input');

  if(input.firstChild.nodeValue == '0' && this.firstChild.nodeValue != '=') input.innerHTML = '';

  if(this.firstChild.nodeValue == '=')
  {
    input.innerHTML = eval(input.firstChild.nodeValue);
    input.innerHTML += " "+spoons;
    check=true;
  }
  else if(this.firstChild.nodeValue == 'C') input.innerHTML = '0';
  else
  {
    if(check)
    {
      input.innerHTML = '';
      check=false;
    }
    input.innerHTML += this.firstChild.nodeValue;
  }
  //console.log(input.firstChild.nodeValue);
}
var toggleClass = function() {

	var navDiv = document.getElementById("navDiv");

	if (navDiv.className === "navb") {
		navDiv.className += " responsive";
	}
	else {
		navDiv.className = "navb";
	}
}
var changeColor = function(){
  console.log(this.innerHTML);
  if(this.innerHTML=='Dessert Spoon Red')
  {
    $("calcHeading").style.color = "white";
    $("buttons").style.backgroundColor  = "#661616";
    $("aside").style.backgroundColor  = "#661616";
    $("slides").style.backgroundColor  = "#bf4646";
    $("caclulator").style.backgroundColor  = "#bf4646";
    var slides = $("slides").getElementsByClassName('slide')
    for(var i=0; i<slides.length; i++)
    {
      slides[i].style.backgroundColor  = "#bf4646";
    }
    document.body.style.backgroundImage = 'linear-gradient(#661616, lightgrey, #661616)';
  }
  if(this.innerHTML=='Salad Spoon Green')
  {
    $("calcHeading").style.color = "white";
    $("buttons").style.backgroundColor  = "#0f4216";
    $("aside").style.backgroundColor  = "#0f4216";
    $("slides").style.backgroundColor  = "#0f4216";
    $("caclulator").style.backgroundColor  = "#0f4216";
    var slides = $("slides").getElementsByClassName('slide')
    for(var i=0; i<slides.length; i++)
    {
      slides[i].style.backgroundColor  = "#376d3d";
    }
    document.body.style.backgroundImage = 'linear-gradient(#0f4216, lightgrey, #0f4216)';
  }
  if(this.innerHTML=='Granola Lid Black')
  {
    $("calcHeading").style.color = "white";
    $("buttons").style.backgroundColor  = "black";
    $("aside").style.backgroundColor  = "black";
    $("slides").style.backgroundColor  = "black";
    $("caclulator").style.backgroundColor  = "black";
    var slides = $("slides").getElementsByClassName('slide')
    for(var i=0; i<slides.length; i++)
    {
      slides[i].style.backgroundColor  = "#3f3f3f";
    }
    document.body.style.backgroundImage = 'linear-gradient(black, lightgrey, black)';
  }
  if(this.innerHTML=='Canteen Spoon White')
  {
    $("calcHeading").style.color = "#2a8093";
    $("buttons").style.backgroundColor  = "grey";
    $("aside").style.backgroundColor  = "grey";
    $("slides").style.backgroundColor  = "grey";
    $("caclulator").style.backgroundColor  = "grey";
    var slides = $("slides").getElementsByClassName('slide')
    for(var i=0; i<slides.length; i++)
    {
      slides[i].style.backgroundColor  = "lightgrey";
    }
    document.body.style.backgroundImage = 'linear-gradient(#e4ede3, white, #e4ede3)';
  }
  if(this.innerHTML=='Reset')
  {
    $("calcHeading").style.color = "white";
    $("buttons").style.backgroundColor  = "#2a8093";
    $("aside").style.backgroundColor  = "#2a8093";
    $("slides").style.backgroundColor  = "#2a8093";
    $("caclulator").style.backgroundColor  = "#126d7c";
    var slides = $("slides").getElementsByClassName('slide')
    for(var i=0; i<slides.length; i++)
    {
      slides[i].style.backgroundColor  = "#3394a5";
    }
    document.body.style.backgroundImage = 'linear-gradient(#154249, #3394a5, #154249)';
  }
}


window.onload = function() {

  var grid = $("caclulator");
  var items = grid.getElementsByClassName('caclulator-button')
  for(var i=0; i<items.length; i++)
  {
    items[i].onclick = calulate;
  }

  $("barIcon").onclick = toggleClass;

  var buttons = $("buttons").getElementsByClassName('button')
  for(var i=0; i<buttons.length; i++)
  {
    buttons[i].onclick = changeColor;
  }
};
